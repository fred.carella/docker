# plantuml
- [home page : install...](https://plantuml.com/starting)
## build
```bash
docker build -t fcarella/plantuml .
```
## push
```bash
docker image push fcarella/plantuml
```
## run
- best way: run it using docker compose file in the "run" folder
- or, in a pinch, some variation of this
```bash
# access in your browser with url:: http://localhost:8080
docker run -d -p 8080:8080 fcarella/plantuml

```
