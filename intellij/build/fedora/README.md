# fedora docker image for intellij
## create fcarella/intellij:2023.1
- this builds and runs in the dockerized intellij image.
- the original project would crash intellij I dont know why.
- this app is proof of concept of running intellij in a docker image along with accessing a dockerized mysql image
## uses fcarella/fedora-java-base:36 as its base image
- installs intellij 2023.1 on top of that
## build
```bash
docker build -t fcarella/intellij:2023.1 .
```
## push
```bash
docker image push fcarella/intellij:2023.1
```
## run
- best way: run it using docker compose file in the "run" folder
- or, in a pinch, some variation of this
```bash
docker run --rm -it -v /c/Users/fcarella/IntellijProjects:/root/IntellijProjects -e DISPLAY=192.168.0.158:0.0 fcarella/intellij:2023.1
```
