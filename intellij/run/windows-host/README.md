# run netbeans in a windows host
## runs docker image fcarella/netbeans:17 with docker compose
## how to run (note** when you run it Netbeans 17 should open in a window)
- best way: run it using docker compose file in the "run" folder
```bash
docker compose up
```
- NOTE** 
  - the ip of the mysql server is:
  ```
  172.28.1.1
  ```
  - if you get a network error, for example
  ```
  - Network windows-host_testing_net  Error                                                                     0.0s 
failed to create network windows-host_testing_net: Error response from daemon: Pool overlaps with other one on this 
address space
  ```
  - then prune the network
  ```bash 
  docker network prune
  ```

- or, in a pinch, some variation of this
```bash
docker run --rm -it -v /c/Users/fcarella/volumes/intellij/IdeaProjects:/root/IdeaProjects -e DISPLAY=192.168.0.158:0.0 fcarella/intellij:2023.1
```

## to find the iup of the mysql server
```bash
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' csd121-mysql
the ip, for example 172.17.0.2
```
