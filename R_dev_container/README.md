# visual studio code base
- R application development using [r-base offcial image](https://hub.docker.com/_/r-base)
- Tutorial : [Milos Makes : Create crisp topographic maps with R...](https://www.youtube.com/watch?v=zoLChBALc1k)
 - Download main.r from [here](https://github.com/milos-agathon/create-crisp-topographic-maps-with-r/tree/main/R)
 - run it in a terminal
 ```bash
 Rscript main.r
 ```
- [Dev Containers tutorial...](https://code.visualstudio.com/docs/devcontainers/tutorial)
- <ctrl><shift><p>dev containers: open folder in container:
  - reads .devcontainer.json for instructions

## Build notes
- to get rid of 'elevatr not found errors' I modified line 15 in main.r from this 
>install.packages(libs[!installed_libs])
>
to this
>install.packages(libs[!installed_libs], repos = 'https://cran.r-project.org')
>
