# Detailed hillshading anywhere in the world! with QGIS
- [Detailed hillshading anywhere in the world!](https://www.maptiler.com/news/2022/06/detailed-hillshading-anywhere-in-the-world/)
- [How to print and show vector tiles in QGIS via MapTiler plugin...](https://documentation.maptiler.com/hc/en-us/articles/4408609607825-How-to-print-and-show-vector-tiles-in-QGIS-via-MapTiler-plugin)
- [qgis maptiler keys...](https://cloud.maptiler.com/account/keys/?utm_campaign=qgis-plugin&utm_medium=product&utm_source=qgis)
- [3D printing of digital elevation models with QGIS...](https://edutechwiki.unige.ch/en/3D_printing_of_digital_elevation_models_with_QGIS)