# run in a windows host
- you must edit the DISPLAY ip in docker-compose.yml for your own usage
## runs docker image fcarella/qgis:release-3_30 with docker compose
- NOTE the security password, the mysql password, all passwords  = "itstudies12345"
## how to run (note** when you run it Netbeans 17 should open in a window)
- best way: run it using docker compose file in the "run" folder
```bash
docker compose up
```

- or, run with some variation of this command
```bash
docker run --rm -it -v /c/Users/fcarella/volumes/qgis:root/ -e DISPLAY=192.168.0.158:0.0 fcarella/qgis:release-3_30
```

# once running, execute qgis in the shell
```bash
qgis
```
