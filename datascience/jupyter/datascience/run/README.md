# clone freds notebooks
[git repo...](https://gitlab.com/fred.carella/jupyter-notebooks)
# to run
```bash
docker compose up
```
- access jupyter through the browser
  - get the servers IP : the servers IP is statically configured in the compose file to be
    ```bash
    IP = 172.28.1.2
    ```
  - then find the URL and token
    - view the container logs
      - in vscode, right click on the container node and choose "View Logs"
      - you will see something like

``` 
To access the server, open this file in a browser:
    file:///home/jovyan/.local /share/jupyter/runtime/jpserver-7-open.html
Or copy and paste one of these URLs:
    http://6c0d674777e8:8888/lab?token=84c807c6696fa3e22552f922d9dcbd76b0b1bce976440a48  
    http://127.0.0.1:8888/lab?token=84c807c6696fa3e22552f922d9dcbd76b0b1bce976440a48 
```
  - then open this URL in the browser
  ```
  http://127.0.0.1:8888/lab?token=84c807c6696fa3e22552f922d9dcbd76b0b1bce976440a48 
  ```
  - another way to find the token is to open up a shell on the container
```bash
docker exec -ti jupyter-datascience-latest /bin/bash
jovyan@f90cb78d3be6:~$  jupyter server list
[JupyterServerListApp] Currently running servers:
[JupyterServerListApp] http://f90cb78d3be6:8888/?token=b7e7744825f00a85b883b3751e18007575dbedc7f460ecb0 :: /home/jovyan
```
    - then use the URL is the browser
``` 
http://localhost:8888/?token=b7e7744825f00a85b883b3751e18007575dbedc7f460ecb0
```
- if you need to get a shell on the container then:
```bash
docker exec -ti jupyter-datascience-latest /bin/bash
