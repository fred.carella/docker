# jupiter-datascience
- jupyter notebook for datascience

## build
```bash
docker build -t fcarella/jupyter-datascience:latest .
```
## push
```bash
docker image push fcarella/jupyter-datascience:latest
```