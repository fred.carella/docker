# exploring data science thru jupyter notebooks
- [git repo...](https://gitlab.com/fred.carella/jupyter-notebooks.git)
- [Towards Data Science : How to Run Jupyter Notebook on Docker...](https://towardsdatascience.com/how-to-run-jupyter-notebook-on-docker-7c9748ed209f)
- [Jupyter Docker Stacks...](https://jupyter-docker-stacks.readthedocs.io/en/latest/)
- [Earth Data Analytics Online Certificate...](https://www.earthdatascience.org/courses/use-data-open-source-python/)
  - [Earth Data Science Textbook...](https://www.earthdatascience.org/courses/intro-to-earth-data-science/)
- [Learn Python with Jupyter...](https://learnpythonwithjupyter.com/assets/book/learn_python_with_jupyter.pdf)
