# fedora docker image for qt
- NOTE ** qt must be installed manually because of licensing issues
  - run this image and then install qt using the online installer as described below
  - you login with your qt credentials and install the qt components you want into the default folder of /opt/Qt/  /opt/Qt is mapped back to the host volume: /c/Users/fcarella/opt_qt
### [see...](https://flames-of-code.netlify.app/blog/qt-on-docker/)
## create fcarella/qt:6
## uses fcarella/fedora-java-base:36 as its base image
- installs qt 6 on top of that
## build
```bash
docker build -t fcarella/qt:6.5 .
```
## push
```bash
docker image push fcarella/qt:6.5
```
## run
- best way: run it using docker compose file in the "run" folder
- or, in a pinch, some variation of this
```bash
docker run --rm -it -v /c/Users/fcarella/volumes/qt/opt:/opt -v /c/Users/fcarella/volumes/qt/root:/root -e DISPLAY=192.168.0.158:0.0 fcarella/qt:6  
```

- run qt online installer
- [mirror list...](https://download.qt.io/static/mirrorlist/)
  - use faster mirror
```bash
./qt-unified-linux-x64-online.run --mirror https://mirrors.ocf.berkeley.edu/qt/
```
  - use mirror
```bash
./qt-unified-linux-x64-online.run --mirror http://www.nic.funet.fi/pub/mirrors/download.qt-project.org
```
```bash
./qt-unified-linux-x64-online.run --mirror https://qt.mirror.constant.com/
```

