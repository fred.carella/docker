# fedora-r:38
- fedora minimal 38
- R
## build
```bash
docker build -t fcarella/fedora-r:38 .
```
## push
```bash
docker image push fcarella/fedora-r:38
```