# fedora-R
- for R development
- ref 1 : [Fedora Packages of R Software...](https://cran.r-project.org/bin/linux/fedora/)
- fedora 38 minimal
  - see Docker file for details
  
- Tutorial : [Milos Makes : Create crisp topographic maps with R...](https://www.youtube.com/watch?v=zoLChBALc1k)
 - Download main.r from [here](https://github.com/milos-agathon/create-crisp-topographic-maps-with-r/tree/main/R)
 - run it in a terminal
 ```bash
 Rscript main.r
 ```
- [Dev Containers tutorial...](https://code.visualstudio.com/docs/devcontainers/tutorial)
- <ctrl><shift><p>dev containers: open folder in container:
  - reads .devcontainer.json for instructions
