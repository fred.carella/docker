# jenkins
- [docker install...](https://www.jenkins.io/doc/book/installing/docker/)
```bash
docker network create jenkins
```
- Customize the official Jenkins Docker image, by executing the following two steps:
  - Create a Dockerfile with the following content:
```bash
FROM jenkins/jenkins:2.426.2-jdk17
USER root
RUN apt-get update && apt-get install -y lsb-release
RUN curl -fsSLo /usr/share/keyrings/docker-archive-keyring.asc \
  https://download.docker.com/linux/debian/gpg
RUN echo "deb [arch=$(dpkg --print-architecture) \
  signed-by=/usr/share/keyrings/docker-archive-keyring.asc] \
  https://download.docker.com/linux/debian \
  $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list
RUN apt-get update && apt-get install -y docker-ce-cli
USER jenkins
RUN jenkins-plugin-cli --plugins "blueocean docker-workflow"
```
- build it
```bash
docker build -t myjenkins-blueocean:2.426.2-1 .
```
- run it
  ```bash
  docker run \
  --name jenkins-blueocean \
  --restart=on-failure \
  --detach \
  --network jenkins \
  --env DOCKER_HOST=tcp://docker:2376 \
  --env DOCKER_CERT_PATH=/certs/client \
  --env DOCKER_TLS_VERIFY=1 \
  --publish 8080:8080 \
  --publish 50000:50000 \
  --volume jenkins-data:/var/jenkins_home \
  --volume jenkins-docker-certs:/certs/client:ro \
  myjenkins-blueocean:2.426.2-1
  ```
  - attach a shell to the running container. 
  - find the admin (root) password
```bash
cat /var/jenkins_home/secrets/initialAdminPassword
```
- Browse to http://localhost:8080 (or whichever port you configured for Jenkins when installing it) and wait until the Unlock Jenkins page appears.
- login with the admin password
- install recommended plugins
- create admin user, fcarella password: itstudies12345