# jenkins
- [docker install...](https://www.jenkins.io/doc/book/installing/docker/)
```bash
docker network create jenkins
```
```bash
docker run \
  --name jenkins-docker \
  --rm \
  --detach \
  --privileged \
  --network jenkins \
  --network-alias docker \
  --env DOCKER_TLS_CERTDIR=/certs \
  --volume jenkins-docker-certs:/certs/client \
  --volume jenkins-data:/var/jenkins_home \
  --publish 2376:2376 \
  docker:dind \
  --storage-driver overlay2
```
- [post installation...](https://www.jenkins.io/doc/book/installing/docker/#setup-wizard)
  - Browse to http://localhost:8080