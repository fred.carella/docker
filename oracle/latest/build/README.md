# docker image for oracle database free
## create fcarella/oracle-free:latest
## build
```bash
docker build -t fcarella/oracle-free:latest .
```
## push
```bash
docker image push fcarella/oracle-free:latest
```
## run
- best way: run it using docker compose file in the "run" folder
- or, in a pinch, some variation of this
```bash
docker run --rm -p 1521:1521  -e ORACLE_PWD=Sirius1001 -v /home/fcarella/volumes/oracle/oradata:/opt/oracle/oradata fcarella/oracle-free:latest
```
