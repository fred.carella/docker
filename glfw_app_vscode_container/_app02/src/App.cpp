#include "App.hpp"
App::App()
{

}
/// @brief 
App::~App()
{

}
int App::run()
{
    std::cout << "_app02 running..." << std::endl; 
    Car mustang("Ford", "Mustang", 2023);
    Car camaro("Chevrolet", "Camaro", 2020);
    std::cout << "Cars = " << std::endl << mustang << std::endl << camaro << std::endl;

    return 1;
}