#include "Car.hpp"

// overload << equivalent to java toString
std::ostream &operator<<(std::ostream &s, const Car &car)
{
    return s << "(" << car.make << ", " << car.model << ", " << car.year << ")" << std::endl;
}
Car::Car() : make(""), model(""), year(1900)
{
}
Car::Car(std::string m, std::string mo, int yr) : make(m), model(mo), year(yr)
{
}
