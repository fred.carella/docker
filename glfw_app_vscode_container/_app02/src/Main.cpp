#include <iostream>
#include "Car.hpp"
#include "App.hpp"

int main()
{
    App app;
    app.run();
    return 0;
}
