#ifndef APP_H
#define APP_H
#include <iostream>
#include "Car.hpp"

class App
{
private:
    Car *cars=new Car[10];

public:
    App();
    ~App();
    int run();
};
#endif
