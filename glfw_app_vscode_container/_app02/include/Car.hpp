#ifndef CAR_HPP
#define CAR_HPP
#include <iostream>
#include "stdio.h"
class Car
{
private:
    std::string make;
    std::string model;
    int year;

public:
    // equivalent to toString
    friend std::ostream &operator<<(std::ostream &s, const Car &car);

    Car();
    Car(std::string m, std::string mo, int yr);
};
#endif
