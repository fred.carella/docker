#ifndef APP_H
#define APP_H
#include <iostream>
#include "G.hpp"

class App
{
private:

public:
    App();
    ~App();
    int run();
};
#endif
