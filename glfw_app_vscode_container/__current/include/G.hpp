#ifndef G_H
#define G_H
#include "glad/glad.h"
#include <GLFW/glfw3.h>

#include "shader_s.h"

#include <iostream>
class G
{
    
    private:
        int i;
        // settings
        const unsigned int SCR_WIDTH = 800;
        const unsigned int SCR_HEIGHT = 600;
        GLFWwindow* window;
        unsigned int VBO, VAO;
        // std::string shader_vs;
        // std::string shader_fs;
        const char* vertexPath;
        const char* fragmentPath;
        void init();


 
    public:
    // equivalent to toString
    friend std::ostream &operator<<(std::ostream &s, const G &g);
    G(int i);
    // G(std::string vs, std::string fs);
    G(const char* vs, const char* fs);
    G();
    ~G();
    void render();

    
};
#endif