# fedora docker image for geogram
## Implements Quaoar Lesson 6: OpenCascade in Docker | Why Docker | Linux and Windows images
- [my instructions...](https://docs.google.com/document/d/1ypBugBimos26U40Ctg95ACQMwCtQ7-V_9ZUGhVP1tPM/edit?usp=sharing)
## uses fcarella/fedora-cpp-base:36 as its base image
- installs geogram on top of that
## build
```bash
docker build -t fcarella/geogram:latest .
```
## push
```bash
docker image push fcarella/geogram:latest
```
## run
- best way: run it using docker compose file in the "run" folder
- or, in a pinch, some variation of this
```bash
docker run --rm -it -e DISPLAY=192.168.0.158:0.0 fcarella/geogram:latest
```

