# run geogram
## how to run
- best way: run it using docker compose file in the "run" folder
```bash
docker-compose run --rm geogram:latest
```


- or, run with some variation of this command
```bash
docker run --rm -it -e DISPLAY=192.168.0.158:0.0 fcarella/geogram:latest
```
