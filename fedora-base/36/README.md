# fedora-java:36
- fedora minimal 36
- git
- x windows
- opengl
- various stuff: xclock, fonts, see build file
## build
```bash
docker build -t fcarella/fedora-base:36 .
```
## push
```bash
docker image push fcarella/fedora-base:36
```