# fedora docker image for netbeans
## create fcarella/netbeans:15
## uses fcarella/netbeans:15 as its base image
- installs jeddict 6.1.0 plugin on top of that
## build
```bash
docker build -t fcarella/jeddict:6.1.0 .
```
## push
```bash
docker image push fcarella/jeddict:6.1.0
```
## copy and unzip root.zip into the mounted volume
- do this after running the container with docker compose up
```bash
copy .\root.zip C:\Users\fcarella\volumes\jeddict6.1.0
or
cp ./root.zip $HOME/volumes/jeddict-6.1.0/
# unzip into above folder using any method you know

```
## run
- best way: run it using docker compose file in the "run" folder
- or, in a pinch, some variation of this
```bash
docker run --rm -it -v /c/Users/fcarella/volumes/NetbeansProjects_jeddict1.1.0:/root/NetBeansProjects -e DISPLAY=192.168.0.158:0.0 fcarella/jeddict:6.1.0
```
