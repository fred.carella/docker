# freecad .20.2 
- [FreeCAD_0.20.2-2022-12-27...](https://github.com/FreeCAD/FreeCAD/releases/download/0.20.2/FreeCAD_0.20.2-2022-12-27-conda-Linux-x86_64-py310.AppImage)

## build
```bash
docker build -t fcarella/freecad:20.2 .
```
## push
```bash
docker image push fcarella/freecad:20.2
```
## run
- best way: run it using docker compose file in the "run" folder
- or, in a pinch, some variation of this
```bash
docker run --rm -it -v /c/Users/fcarella/FreecadProjects:/root/ -e DISPLAY=192.168.0.158:0.0 fcarella/freecad:20.2
```
