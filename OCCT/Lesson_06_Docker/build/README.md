# fedora docker image for Opencascade
## Implements Quaoar Lesson 6: OpenCascade in Docker | Why Docker | Linux and Windows images
- [my instructions...](https://docs.google.com/document/d/1dFU1hBPy3BFEcbDITmjz0Ygc72yFc8L5FSwgFoWnffU/edit#heading=h.ketferyvqs67)
## create fcarella/quaoarlesson06-fedora:36:_occt7.6
## uses fcarella/fedora-java-base:36 as its base image
- installs netbeans 17 on top of that
## build
```bash
docker build -t fcarella/quaoarlesson:6 .
```
## push
```bash
docker image push fcarella/quaoarlesson:6
```
## run
- best way: run it using docker compose file in the "run" folder
- or, in a pinch, some variation of this
```bash
docker run --rm -it -e DISPLAY=192.168.0.158:0.0 fcarella/quaoarlesson:6
```

