# run Opencascade draw.sh in a windows host
- you must edit the DISPLAY ip in docker-compose.yml for your own usage
## runs docker image fcarella/quaoarlesson:6 with docker compose
- NOTE the security password, the mysql password, all passwords  = "itstudies12345"
## how to run
- best way: run it using docker compose file in the "run" folder
```bash
docker-compose run --rm quaoarlesson_6
```


- or, run with some variation of this command
```bash
docker run --rm -it -e DISPLAY=192.168.0.238:0.0 fcarella/quaoarlesson:6
# then in the shell
/usr/bin/draw.sh
```
