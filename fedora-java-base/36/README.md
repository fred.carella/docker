# fedora-java:36
- fedora minimal 36
- latest openjdk
- git
- x windows
- opengl

## build
```bash
docker build -t fcarella/fedora-java-base:36 .
```
## push
```bash
docker image push fcarella/fedora-java-base:36
```