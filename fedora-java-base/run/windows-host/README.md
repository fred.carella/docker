# to run
```bash
docker compose up
```
- then get a shell on the container with:
```bash
docker exec -ti fedora-java-base-36 /bin/bash
```
## java versions
- jdk 11 and jdk-latest (19 as of this writing) are installed, but default is jdk 11
- to change default jdk, use the alternatives command in the docker build file to set the default