
# Consider dependencies only in project.
set(CMAKE_DEPENDS_IN_PROJECT_ONLY OFF)

# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  )

# The set of dependency files which are needed:
set(CMAKE_DEPENDS_DEPENDENCY_FILES
  "/workspaces/docker/cherno_c++_dev_container/01_How_to_Setup_C++_on_Linux/main.cpp" "01_How_to_Setup_C++_on_Linux/CMakeFiles/_app01.dir/main.cpp.o" "gcc" "01_How_to_Setup_C++_on_Linux/CMakeFiles/_app01.dir/main.cpp.o.d"
  )

# Targets to which this target links which contain Fortran sources.
set(CMAKE_Fortran_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
