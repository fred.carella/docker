# fcarella/octoprint:latest
- octoprint for plugin dev
## build
```bash
docker build -t fcarella/octoprint:latest .
```
## push
```bash
docker image push fcarella/octoprint:latest
```