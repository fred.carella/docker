# to run
```bash
docker compose up
```
- then get a shell on the container with:
```bash
docker exec -ti fedora-base-36 /bin/bash
```
- or use VS Code interface to open shell

- access octoprint
  - get the ip of the server by opening a shell and then 
  ```bash
  ip a
  ```
  - open URL, ex;
  ```
  http://192.168.0.241/
  ```
