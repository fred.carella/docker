# fedora docker image for netbeans
## create fcarella/netbeans:17_cpp
## uses fcarella/fedora-cpp-base:36 as its base image
- installs netbeans 17 on top of that
## helpful tips
- [check for file changes...](https://unix.stackexchange.com/questions/453385/shell-check-if-any-file-in-directory-has-changed)

## build
```bash
docker build -t fcarella/netbeans:17_cpp .
```
## push
```bash
docker image push fcarella/netbeans:17_cpp
```
## run
- best way: run it using docker compose file in the "run" folder
- or, in a pinch, some variation of this
```bash
docker run --rm -it -v /c/Users/fcarella/NetBeansProjects:/root/NetBeansProjects -e DISPLAY=192.168.0.158:0.0 fcarella/netbeans:17_cpp
```
