# run netbeans in a windows host
- you must edit the DISPLAY ip in docker-compose.yml for your own usage
## runs docker image fcarella/netbeans:17 with docker compose
- NOTE the security password, the mysql password, all passwords  = "itstudies12345"
## how to run (note** when you run it Netbeans 17 should open in a window)
- best way: run it using docker compose file in the "run" folder
```bash
docker compose up
```
- NOTE** 
  - the ip of the mysql server is:
  ```
  172.28.1.1
  ```
  - if you get a network error, for example
  ```
  ... failed to create network windows-host_testing_net: Error response from daemon: Pool overlaps with other one on this address space
  ```
  - then prune the network
  ```bash 
  docker network prune
  ```

- or, run with some variation of this command
```bash
docker run --rm -it -v /c/Users/fcarella/NetBeansProjects:/root/NetbeansProjects -e DISPLAY=192.168.0.158:0.0 fcarella/netbeans:17
```

- on archy this command works
```bash
docker run --rm -it --network=host -v /c/Users/fcarella/NetBeansProjects:/root/NetBeansProjects -e DISPLAY=$DISPLAY fcarella/netbeans:17
```

## to find the iup of the mysql server
```bash
docker inspect -f '{{range.NetworkSettings.Networks}}{{.IPAddress}}{{end}}' csd121-mysql
the ip, for example 172.17.0.2
```
