# fedora docker image for netbeans
## create fcarella/netbeans:15
## uses fcarella/fedora-java-base:36 as its base image
- installs netbeans 15 on top of that
## build
```bash
docker build -t fcarella/netbeans:15 .
```
## push
```bash
docker image push fcarella/netbeans:15
```
## run
- best way: run it using docker compose file in the "run" folder
- or, in a pinch, some variation of this
```bash
docker run --rm -it -v /c/Users/fcarella/NetBeansProjects:/root/NetBeansProjects -e DISPLAY=192.168.0.158:0.0 fcarella/netbeans:15
```
