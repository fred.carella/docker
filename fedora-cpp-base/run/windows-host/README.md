# to run
```bash
docker compose up
```
- then get a shell on the container with:
```bash
docker exec -ti fedora-cpp-base-36 /bin/bash
```