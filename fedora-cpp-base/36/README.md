# fedora-java:36
- fedora minimal 36
- install "Development Tools" group manually since microdnf does not support groups
  - dnf groupinfo "Development Tools"
- git
- x windows
- opengl

## build
```bash
docker build -t fcarella/fedora-cpp-base:36 .
```
## push
```bash
docker image push fcarella/fedora-cpp-base:36
```